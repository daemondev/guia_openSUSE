El propósito de este proyecto es traducir la guía no oficial de openSUSE desarrollada por un usuario, disponible en inglés en esta dirección.

* [http://www.opensuse-guide.org/index.php](http://www.opensuse-guide.org/index.php)

El proyecto original está desarrollado y mantenido por __Martin Schlander__ bajo licencia ["GNU Free Documentation License".](https://www.gnu.org/licenses/fdl.html)

Agradecer el trabajo de __jcsl__ en labores de depuración de código y mejoras sustanciales en toda la guía.

Se agradece el feedback y las posibles colaboraciones. Puedes encontrar la página de la guía según se vaya traduciendo en este enlace:
* [https://victorhck.gitlab.io/guia_openSUSE/](https://victorhck.gitlab.io/guia_openSUSE/)

[Victorhck](https://victorhckinthefreeworld.com/)
